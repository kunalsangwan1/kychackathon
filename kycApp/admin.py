from django.contrib import admin
from kycApp.models import Otps1, UserInfo, FacebookConnection

# Register your models here.
class OTPAdmin(admin.ModelAdmin):
    list_display = ('aadhar',)
admin.site.register(Otps1, OTPAdmin)


class UserInfoAdmin(admin.ModelAdmin):
    list_display = ('name', 'aadhar')
admin.site.register(UserInfo, UserInfoAdmin)
