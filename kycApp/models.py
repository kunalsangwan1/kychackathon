from django.db import models

# Create your models here.
# class Otps(models.Model):
#     aadhar = models.CharField(max_length=50, unique=True)
#     otp = models.CharField(max_length=6, default="")
#     
#     def __unicode__(self):
#         return self.user.email
#     
#     class Meta:
#         verbose_name = 'OTP'
#         verbose_name_plural = 'OTPs'
        
        
class Otps1(models.Model):
    aadhar = models.CharField(max_length=50, unique=True)
    otp = models.CharField(max_length=6, default="")
    
    def __unicode__(self):
        return self.aadhar
    
    class Meta:
        verbose_name = 'OTP'
        verbose_name_plural = 'OTPs'
        

class UserInfo(models.Model):
    aadhar = models.CharField(max_length=50, unique=True)
    name = models.CharField(max_length=50)
    address = models.CharField(max_length=50)
    dob = models.CharField(max_length=20)
    gender = models.CharField(max_length=10)
    phone = models.CharField(max_length=10)
    email = models.CharField(max_length=10)
#     photo

    def __unicode__(self):
        return self.name
    
    class Meta:
        verbose_name = 'Info'
        verbose_name_plural = 'Info'
        
        
class FacebookConnection(models.Model):
    aadhar = models.CharField(max_length=30, blank=True, null=True)
    base_user = models.CharField(max_length=30, blank=True, null=True)
    friend_user = models.CharField(max_length=30, blank=True, null=True)
    
    def __unicode__(self):
        return self.base_user
    
    class Meta:
        verbose_name = 'Facebook Connection'
        verbose_name_plural = 'Facebook Connections '
