from rest_framework import serializers

import json
from rest_framework.renderers import JSONRenderer
from django.contrib.auth.models import User
from kycApp.models import UserInfo
 
class UserInfoSerializer(serializers.ModelSerializer):
    @staticmethod
    def get_serialized(user_info):
        serialized_data = UserInfoSerializer(user_info).data
        return json.loads(JSONRenderer().render(serialized_data))
     
    class Meta:
        model = UserInfo
             
