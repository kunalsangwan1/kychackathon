from rest_framework.decorators import api_view
from kycApp.builder.result_builder import ResultBuilder
from kycApp.helper.util import get_or_none
from kycApp.models import Otps1, UserInfo, FacebookConnection
from random import randint
from kycApp.serializers import UserInfoSerializer
from kycApp.helper import aadhar_helper, salesforce_helper, util
import requests


@api_view(['POST'])
def get_OTP(request):
    result_builder = ResultBuilder()
    post_data = request.data
    aadhar = post_data.get("aadhar")
 
    otps = {"555555555555" : "123456",
           "444444444444" : "123457",
           "333333333333" : "123458"}
       
    otp = otps.get(aadhar)
    user_otp = get_or_none(Otps1, aadhar=aadhar)
    print "######***********", post_data
    
    if not user_otp:
        Otps1.objects.create(aadhar=aadhar,
                            otp=otp)
    else:
        user_otp.otp = otp
        user_otp.save()
    
    result_builder.success().ok_200().message("OTP Sent").result_object({"otp":str(otp)});
    
    return result_builder.get_response()


@api_view(['POST'])
def submit_aadhar(request):
    result_builder = ResultBuilder()
    post_data = request.data
    aadhar = post_data.get('aadhar')
    
    if aadhar_helper.verify_aadhar(aadhar): 
        result_builder.success().ok_200().message("Aadhar Accepted").result_object("{Need second factor authentication}");
    else:
        result_builder.fail().unauthorized_401_invalid_input().message("Invalid Aadhar");
        
    return result_builder.get_response()


@api_view(['POST'])
def verify_with_otp(request):
    result_builder = ResultBuilder()
    user = request.user
    post_data = request.data
    aadhar = post_data.get('aadhar')
    
    if aadhar_helper.verify_otp(aadhar, post_data.get('otp')):
        result_builder.success().ok_200().message("Aadhar Accepted").result_object({"data":aadhar_helper.get_aadhar_info(aadhar)});
        return result_builder.get_response()
    else:
        result_builder.fail().user_unauthorized_401().message("Invalid otp");
        return result_builder.get_response()
    

@api_view(['POST'])
def verify_with_details(request):
    result_builder = ResultBuilder()
    user = request.user
    post_data = request.data
    aadhar = post_data.get('aadhar')
    name = post_data.get('name')
    gender = post_data.get('gender')
    dob = post_data.get('dob')
    age = post_data.get('age')
    address = post_data.get('address')
    mobile = post_data.get('mobile')
    
    if aadhar_helper.verify_details(aadhar, name, gender, dob, age, address, mobile):
        access_token = util.get_access_token()
        salesforce_helper.post_customer_data(aadhar, name, address, dob, gender, mobile, access_token)
        result_builder.success().ok_200().message("Aadhar Accepted").result_object({"data":aadhar_helper.get_aadhar_info(aadhar)});
        return result_builder.get_response()
    else:
        result_builder.fail().user_unauthorized_401().message("Invalid otp");
        return result_builder.get_response()
    
    
@api_view(['POST'])
def get_linkedin_data(request):
    result_builder = ResultBuilder()
    post_data = request.data
    
    aadhar = post_data.get('aadhar')
    
    user_info = UserInfo.objects.get(aadhar=aadhar)
    user_info.linkedin_integration = True
    user_info.save()
    
    access_token = "AQUCpSr43nkrWVmm74r2AM9PpdDaEDvcHjC_OCItNuo_p_Wx_y1hMwgu-JdYgJYzcp2G8AB9p25MfPMNAzJ9h57Ic1z7ZPnafC0JxuBkuZ44Mm6ONaondzn2Mn3hZLBfLYAbLPSlCiytqVCbaG_x6vch6-S4EwlvdT82LqmPknMutjiItns"
    url = "https://api.linkedin.com/v1/people/~:(id,num-connections,picture-url,industry,positions)?oauth2_access_token=" + access_token + "&format=json"
    
    response = requests.get(url).json()
    
    total_company = response.get('positions').get('_total')
    
    if int(total_company) > 0: 
        companies = response.get('positions').get('values')
        access_token = util.get_access_token()
        
        for company in companies:   
            company_name = company.get('company').get('name') 
            company_title = company.get('title')
            isCurrent = company.get('isCurrent')
            industry = company.get('company').get('industry')
            
            print aadhar, company_name, company_title, isCurrent, industry
            salesforce_helper.post_user_linkedin_data(aadhar, company_name, company_title, isCurrent, industry, access_token)
            print company_name, company_title, isCurrent, industry
        
    result_builder.success().ok_200().message("Successful");
    return result_builder.get_response()



@api_view(['POST'])
def get_facebook_data(request):
    result_builder = ResultBuilder()
    post_data = request.data
    
    aadhar = post_data.get('aadhar')
    
    user_info = UserInfo.objects.get(aadhar=aadhar)
    user_info.facebook_integration = True
    user_info.save()
    
    access_token = "CAACEdEose0cBADHSeftp7LZBJBcxOMkXTeoKkiCupZBq6Po4vo47rISbay6c1SVSkrBWovwAZBHhOCWQdbz561JoQh199vjFqXVnenCodk44zTjFliOfZC9BHrluTF9LWn4pJOGIT11GzMQkTn44in3ANoIxZBH5xfGnMBM3GpMVthdcgDXZCgZATqDQwvZAgBJ8raoaRCEwjveBLqf1KL0v"
    url = "https://graph.facebook.com/v2.2/me/friends?access_token=" + access_token + "&debug=all&format=json&method=get&pretty=1&suppress_http_code=1&limit=1000&offset=0"
    
    fb_user_id = post_data.get('id')
    
    response = requests.get(url).json()
    print response

    all_connections = response.get('data')
    access_token = util.get_access_token()
    
    for connection in all_connections[:5]:
        fb_id = connection.get('id')
        fb_name = connection.get('name')
        FacebookConnection.objects.get_or_create(base_user=fb_user_id,
                                                 friend_user=fb_id,
                                                 aadhar=aadhar)
        
        print aadhar, fb_user_id, fb_id, fb_name
        salesforce_helper.post_user_connections(aadhar, fb_user_id, fb_id, fb_name, access_token)
        
    
    total_connections_at_barclay = FacebookConnection.objects.filter(friend_user=fb_user_id).count()
    
    result_builder.success().ok_200().message("Successful").result_object({"total_connections":total_connections_at_barclay});
    return result_builder.get_response()
    
    
