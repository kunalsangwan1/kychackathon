'''
Created on 29-Nov-2015

@author: nuhbye
'''
import requests
from kycApp.helper import util
import json
from kycApp.models import FacebookConnection

base_url = "https://ap2.salesforce.com"

def post_user_connections(aadhar, fb_user_id, fb_id, fb_name, access_token):
    new_account = {}
    new_account['aadhar__c'] = aadhar
    new_account['Base_User__c']  = fb_user_id
    new_account['Friend_User__c']  = fb_id
    new_account['Friend_Name__c'] = fb_name
    
    headers = {'Authorization': 'Bearer ' + access_token,
           'Content-type': 'application/json'}
    
    json_new_account = json.dumps(new_account)
    
    requests.post(base_url + "/services/data/v34.0/sobjects/Facebook_Connections__c/", data=json_new_account, headers=headers)


def post_user_linkedin_data(aadhar, company_name, company_title, isCurrent, industry, access_token):
    new_account = {}
    new_account['aadhar__c'] = aadhar
    new_account['CompanyName__c'] = company_name
    new_account['CompanyTitle__c'] = company_title
    new_account['Industry__c'] = industry
    new_account['IsCurrent__c'] = isCurrent
    
    headers = {'Authorization': 'Bearer ' + access_token,
           'Content-type': 'application/json'}
    
    json_new_account = json.dumps(new_account)
    
    requests.post(base_url + "/services/data/v34.0/sobjects/LinkedInConnection__c/", data=json_new_account, headers=headers)
    
    
def post_customer_data(aadhar, name, address, dob, gender, phone, access_token):
    new_account = {}
    new_account['aadhar__c'] = aadhar
    new_account['name__c'] = name
    new_account['address__c'] = address
    new_account['dob__c'] = dob
    new_account['gender__c'] = gender
    new_account['phone__c'] = phone 
    
    headers = {'Authorization': 'Bearer ' + access_token,
           'Content-type': 'application/json'}
    
    json_new_account = json.dumps(new_account)
    
    requests.post(base_url+ "/services/data/v34.0/sobjects/Customer__c/", data=json_new_account, headers=headers)


def create_relations():
    all_users = FacebookConnection.objects.all()
    
    access_token = util.get_access_token()
    
    for user in all_users:
        friends = FacebookConnection.objects.filter(friend_user=user.base_user)
        new_account = {}
        new_account['name__c'] = user.aadhar
        new_account['connectioncount__c'] = friends.count()
        
        print new_account['name__c'], new_account['connectioncount__c']  
        
        headers = {'Authorization': 'Bearer ' + access_token,
           'Content-type': 'application/json'}
    
    json_new_account = json.dumps(new_account)
    
    requests.post(base_url+ "/services/data/v34.0/sobjects/CustomerConnections__c/", data=json_new_account, headers=headers)
        
        
        
            

