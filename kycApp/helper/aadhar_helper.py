'''
Created on 28-Nov-2015

@author: nuhbye
'''
from kycApp.models import Otps1, UserInfo
from kycApp.serializers import UserInfoSerializer


def verify_otp(aadhar, otp):
    user_otp = Otps1.objects.get(aadhar=aadhar)
    
    return user_otp.otp == otp


def get_aadhar_info(aadhar):
    return UserInfoSerializer.get_serialized(UserInfo.objects.get(aadhar=aadhar))


def verify_details(aadhar, name, gender, dob, age, address, mobile):
    user_info = UserInfo.objects.get(aadhar=aadhar)
    
    if (user_info.name == name and user_info.gender == gender and
        user_info.dob == dob and user_info.phone == mobile):
        return True;
    else:
        return False;



def verify_aadhar(aadhar):
    return True


