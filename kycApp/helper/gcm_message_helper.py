from push_notifications.models import GCMDevice

def send_message(user, message):
    device = GCMDevice.objects.filter(user=user)
    device.send_message(message)