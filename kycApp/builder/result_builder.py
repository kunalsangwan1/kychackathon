'''
Created on 29-Mar-2015

@author: nuhbye
'''
from rest_framework import status
from rest_framework.response import Response

class ResultBuilder(object):
    '''
    API response builder
    '''


    def __init__(self):
        self.results = {}
        self.status_code = 1
        self.status_message = "" 
        self.status = status.HTTP_200_OK
        
        
    def fail(self):
        self.status_code = -1
        return self
    
    
    def off_working_hours(self):
        self.status_code = -3
        return self
    
    
    def message(self, status_message):
        self.status_message = status_message
        return self
    
    
    def success(self):
        self.status_code = 1
        return self
    
    
    def ok_200(self):
        self.status = status.HTTP_200_OK
        return self
    
    
    def accepted_202(self):
        self.status = status.HTTP_202_ACCEPTED
        return self
    
    
    def not_found_404(self):
        self.status = status.HTTP_404_NOT_FOUND
        return self
    
    
    def bad_request_400(self):
        self.status = status.HTTP_400_BAD_REQUEST
        return self
    
    
    def user_unauthorized_401(self):
        self.status = status.HTTP_401_UNAUTHORIZED
        return self
    
    
    def user_forbidden_403(self):
        self.status = status.HTTP_403_FORBIDDEN
        return self
    
    
    def result_object(self, result):
        self.results = result
        return self
    
    
    def get_response(self):
        content = {}
        
        content['status-code'] = self.status_code
        content['status-message'] = self.status_message
        content['data'] = self.results
         
        return Response(content, status=self.status)
    
    