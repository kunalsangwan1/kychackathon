from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^v1/get_otp/$', views.get_OTP, name='get_OTP'),
    url(r'^v1/submit_aadhar/$', views.submit_aadhar, name='submit_aadhar'),
    url(r'^v1/verify_with_details/$', views.verify_with_details, name='verify_with_details'),
    url(r'^v1/verify_with_otp/$', views.verify_with_otp, name='verify_with_otp'),
    url(r'^v1/submit_aadhar/$', views.submit_aadhar, name='submit_aadhar'),
]