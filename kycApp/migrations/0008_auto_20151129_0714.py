# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kycApp', '0007_auto_20151129_0436'),
    ]

    operations = [
        migrations.AddField(
            model_name='userinfo',
            name='facebook_integration',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='userinfo',
            name='linkedin_integration',
            field=models.BooleanField(default=False),
        ),
    ]
