# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kycApp', '0009_facebookconnection_aadhar'),
    ]

    operations = [
        migrations.CreateModel(
            name='Otps1',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('aadhar', models.CharField(unique=True, max_length=50)),
                ('otp', models.CharField(default=b'', max_length=6)),
            ],
            options={
                'verbose_name': 'OTP',
                'verbose_name_plural': 'OTPs',
            },
        ),
        migrations.DeleteModel(
            name='Otps',
        ),
    ]
