# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kycApp', '0003_userinfo'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='otps',
            name='user',
        ),
        migrations.RemoveField(
            model_name='userinfo',
            name='user',
        ),
        migrations.AddField(
            model_name='otps',
            name='aadhar',
            field=models.CharField(default=None, max_length=50),
        ),
        migrations.AddField(
            model_name='userinfo',
            name='aadhar',
            field=models.CharField(default=None, max_length=50),
        ),
    ]
