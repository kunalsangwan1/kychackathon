# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kycApp', '0004_auto_20151128_1803'),
    ]

    operations = [
        migrations.AlterField(
            model_name='otps',
            name='aadhar',
            field=models.CharField(unique=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='userinfo',
            name='aadhar',
            field=models.CharField(unique=True, max_length=50),
        ),
    ]
