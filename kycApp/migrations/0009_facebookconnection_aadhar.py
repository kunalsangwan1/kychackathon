# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kycApp', '0008_auto_20151129_0714'),
    ]

    operations = [
        migrations.AddField(
            model_name='facebookconnection',
            name='aadhar',
            field=models.CharField(max_length=30, null=True, blank=True),
        ),
    ]
