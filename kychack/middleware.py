from rest_framework import status
from django.http.response import JsonResponse
import logging
import json

log = logging.getLogger(__name__)
 
class APIParserMiddleware(object):
    def process_request(self, request):
        self.path = request.path
        
        if request.method == 'GET':
            input_request = "[GET] " + json.dumps(request.GET)
        elif request.method == 'POST':
            if request.body:
                input_request = " [POST] " + request.body
            else:
                input_request = json.dumps(request.POST)
        
        request_message = "[PATH] " + str(request.path) + " [REQUEST] " + input_request
        
        log.debug(request_message)

    
    def process_response(self, request, response):
        print self.path
        if hasattr(self, 'path') and not self.path.startswith("/admin/") and "<!DOCTYPE html>" not in response.content:
            request_message = "[REQUEST] [PATH]: " + str(request.path)
            request_message += " [GET] [BODY]: "  + json.dumps(request.GET)

            response_message = "[RESPONSE] [STATUS_CODE] " + str(response.status_code) + " [CONTENT] " + str(response.content)
            log.debug(response_message)

        return response
    
    
    def process_exception(self, request, exception):
        content = {'status-code' : -1}
        
        if type(exception) == KeyError:
            return self.bad_request_400_missing_parameter(content, exception)
            
#         elif type(exception) == InvalidTokenException or type(exception) == InvalidUserException or type(exception) == InvalidDriverException:
#             return self.unauthorized_401_invalid_input(content, exception)
        
        log.exception("[EXCEPTION][500]: " + str(exception))
    
    def bad_request_400_missing_parameter(self, content, exception=None):
        error_message = "[EXCEPTION]: " + str(exception)
        log.error(error_message)

        content['status-message'] = "Missing Parameters"
        
        return JsonResponse(content, status=status.HTTP_400_BAD_REQUEST)
    
    
    def unauthorized_401_invalid_input(self, content, exception):
        error_message = "[EXCEPTION][401]: " + str(exception)
        log.error(error_message)
        
        if exception is not None:
            content['status-message'] = "Unauthorized User"
        else:
            content['status-code'] = exception.args[0]['status-code']
            content['status-message'] = exception.args[0]['status-message']
            
        return JsonResponse(content, status=status.HTTP_401_UNAUTHORIZED)
    
    
    def attribute_error(self, content, request, exception):
        error_message = "[EXCEPTION][400]: " + str(exception)
        log.error(error_message)
        
        content['status-message'] = "Missing Attribute"
        
        return JsonResponse(content, status=status.HTTP_400_BAD_REQUEST)
    
    