
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: kee the secret key used in production secret!
SECRET_KEY = 'aohw8ua*drq29z1&5^*--z^1ambfi#6rnyg*@h@wp3rirb3ly+'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin.apps.SimpleAdminConfig',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'kycApp',
    'adminplus',
    'rest_framework.authtoken',
    'push_notifications',
    'social_auth',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.gzip.GZipMiddleware',
    'log_request_id.middleware.RequestIDMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'kychack.middleware.APIParserMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
#            'rest_framework.authentication.TokenAuthentication',    
    ),
    'DEFAULT_PERMISSION_CLASSES': (
#         'rest_framework.permissions.IsAuthenticated',
    )
}

# AUTH_USER_MODEL = 'kychack.CustomUser'

DEFAULT_AUTHENTICATION = {
#     'DEFAULT_AUTHENTICATION_CLASSES': ('rest_framework.permissions.AllowAny',),
}

CORS_ORIGIN_ALLOW_ALL = True

ROOT_URLCONF = 'kychack.urls'

WSGI_APPLICATION = 'kychack.wsgi.application'

DATABASES = {
    'default': {
       'ENGINE': 'django.db.backends.postgresql_psycopg2',
       'NAME': 'kychack',
#        'USER': 'valuevest',	
       'HOST': '127.0.0.1',
#        'PASSWORD': 'valuevest',
       # 'TEST_NAME': 'auto_tests',
       },
    }

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'filters': {
        'request_id': {
            '()': 'log_request_id.filters.RequestIDFilter'
        },
        'require_debug_false': {
            '()': 'django.utils.log.CallbackFilter',
            'callback': lambda r: not DEBUG
        },
    },
    'formatters': {
        'standard': {
            'format' : "[%(asctime)s] %(levelname)s [%(request_id)s] [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'null': {
            'level':'DEBUG',
            'class':'django.utils.log.NullHandler',
        },
        'logfile': {
            'level':'DEBUG',
            'class':'logging.handlers.TimedRotatingFileHandler',
            'filters': ['request_id'],
            'filename': BASE_DIR + "/logs/logfile",
            'when':'h',
            'backupCount': 100,
            'formatter': 'standard',
        },
        'console':{
            'level':'DEBUG',
            'class':'logging.StreamHandler',
            'filters': ['request_id'],
            'formatter': 'standard'
        },
        'mail_admins': {
        'level': 'ERROR',
        'filters':['require_debug_false'],
        'class': 'django.utils.log.AdminEmailHandler'
        },
    },
    'loggers': {
        'django': {
            'handlers':['console','mail_admins'],
            'propagate': True,
            'level':'WARN',
        },
#         'django.db.backends': {
#             'handlers': ['console'],
#             'level': 'DEBUG',
#             'propagate': False,
#         },
        'kychack': {
            'handlers': ['console', 'logfile'],
            'level': 'DEBUG',
        },
                
        'kychack.middleware': {
            'handlers': ['console', 'logfile'],
            'level': 'DEBUG',
        }
    }
}

PUSH_NOTIFICATIONS_SETTINGS = {
        "GCM_API_KEY": "AIzaSyAKq_TAUpw1CCmmcG9vHqD-G9DiDqFqNgo",
#        "APNS_CERTIFICATE": "/path/to/your/certificate.pem",
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
