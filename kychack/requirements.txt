django==1.8.5
djangorestframework==3.2
django-adminplus==0.3
djorm-ext-pgfulltext==0.10
Celery==3.1
requests==2.8.1
django-celery==3.1
django-extensions==1.5.7
django-log-request-id==1.1.0
django-push-notifications==1.3.1
django-social-auth==0.7.28

# pip install django-log-request-id django-extensions requests django-adminplus