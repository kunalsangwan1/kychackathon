from django.conf.urls import patterns, include, url
from django.contrib import admin
from adminplus.sites import AdminSitePlus
from rest_framework.authtoken import views
 
admin.site = AdminSitePlus()
admin.autodiscover()

urlpatterns = [
    url(r'^', include('kycApp.urls')),
    url(r'^api-token-auth/', views.obtain_auth_token, name='obtain_auth_token'),
    url(r'^admin/', include(admin.site.urls)),
]