(($) ->
	$(document).ajaxStart(
		()->
			$('body').addClass 'loading-body'
	)
	$(document).ajaxStop(
		()->
			$('body').removeClass 'loading-body'
	)
	path = "http://1ad6fd67.ngrok.io/v1/"
	$('.enter-adhar-number').submit(
		(e)->
			e.preventDefault()
			$.ajax(
				method : "POST"
				url: path + 'submit_aadhar/'
				data : {"aadhar":$('.aadhar-input').val()}
				dataType : 'json'
				context: document.body).done((data, textStatus, jqXHR) =>
					console.log data,textStatus,jqXHR
					$('.aadhar-input').attr 'readonly','readonly'
					$('.enter-otp,.ptext,.or-text').removeClass 'hide'
				)
	)
	$('.enter-otp').submit(
		(e)->
			e.preventDefault()
			$.ajax(
				method : "POST"
				url: path + 'verify_with_otp/'
				data : {"aadhar":$('.otp-input').val()}
				dataType : 'json'
				context: document.body).done((data, textStatus, jqXHR) =>
					#$('.login').removeClass 'hide'
					$('.enter-adhar-number .input-group-btn').css({
						'visibility' : 'hidden'
					})
					$('.enter-adhar-number input').css({
						'width' : '113%'
					})
					if(data.data.data.name)
						$('.username').text data.data.data.name
					$('.enter-otp,.ptext').fadeOut 'fast'
					#$('.login').fadeIn('fast')
					console.log data,textStatus,jqXHR
					redirect()
				)
	)
	$('.ptext').click(
		(e)->
			$('.enter-otp,.ptext').fadeOut 'fast'
			$('.verification-method-radio,.tab-content').removeClass 'hide'
			$('.verification-method-radio,.tab-content').fadeIn 'fast'
	)
	$('.dg-form').submit(
		
		(e)->
			e.preventDefault()
			$.ajax(
				method : "POST"
				url: path + 'verify_with_details/'
				data : {"aadhar":$('.otp-input').val()}
				dataType : 'json'
				context: document.body).done((data, textStatus, jqXHR) =>
					#$('.login').removeClass 'hide'
					$('.enter-adhar-number .input-group-btn').css({
						'visibility' : 'hidden'
					})
					$('.enter-adhar-number input').css({
						'width' : '113%'
					})
					$('.enter-otp,.ptext').fadeOut 'fast'
					#$('.login').fadeIn('fast')
					redirect()
				)
	)
	redirect = ()->
		setTimeout(
			()->
				window.location.href = "/eKYC/"
			,3500
		)
		$('.redirect-container').removeClass 'hide'
	$('.linkedin-login').click(
		(e)->
			console.log "amit linkedin"
	)
) jQuery