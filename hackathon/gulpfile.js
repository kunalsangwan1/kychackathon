var gulp = require('gulp');
var browserSync = require('browser-sync');
var jade = require('gulp-jade');
var minifyCss = require('gulp-minify-css');
var runSequence = require('gulp-run-sequence');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var cache = require('gulp-cache');
var stylus = require('gulp-stylus');
var nib = require('nib');
var imageop = require('gulp-image-optimization');
var stylusPath = "./styl/*styl";

 gulp.task('compileJade',function(){
    gulp.src('./index.jade')
        .pipe(jade())
        .pipe(gulp.dest('./'));

    gulp.src('./login.jade')
        .pipe(jade())
        .pipe(gulp.dest('./'));

    gulp.src('./upload.jade')
        .pipe(jade())
        .pipe(gulp.dest('./'));

});


gulp.task('form', function() {
    gulp.watch(stylusPath, ['stylus']);
    gulp.watch("./*jade",['compileJade']);
});
gulp.task('stylus', function() {
    gulp.src(stylusPath)
        .pipe(stylus({
            error: true,
            use: (nib())
        }))
        .pipe(minifyCss())
        .pipe(gulp.dest('./css/'))
        .pipe(browserSync.reload({
            stream: true
        }))
});
gulp.task('dev', function() {
    runSequence(['browser-sync', 'form']);
});



gulp.task('browser-sync', function() {
    browserSync.init(["css/*.css", "./*.js", "js/*.js", "**/*.html"], {
        server: {
            baseDir: "./"
        },
        notify: false,
        ghostMode: false
    });
});



gulp.task('autoprefixer', function() {
    var postcss = require('gulp-postcss');
    var sourcemaps = require('gulp-sourcemaps');
    var autoprefixer = require('autoprefixer-core');

    return gulp.src('./css/style2.css')
        .pipe(sourcemaps.init())
        .pipe(postcss([autoprefixer({
            browsers: ['last 7 versions', 'Firefox < 20', 'iOS 7']
        })]))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./css'));
});
