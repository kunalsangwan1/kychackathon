// Generated by CoffeeScript 1.9.3
(function() {
  (function($) {
    var path, redirect;
    $(document).ajaxStart(function() {
      return $('body').addClass('loading-body');
    });
    $(document).ajaxStop(function() {
      return $('body').removeClass('loading-body');
    });
    path = "http://1ad6fd67.ngrok.io/v1/";
    $('.enter-adhar-number').submit(function(e) {
      e.preventDefault();
      return $.ajax({
        method: "POST",
        url: path + 'submit_aadhar/',
        data: {
          "aadhar": $('.aadhar-input').val()
        },
        dataType: 'json',
        context: document.body
      }).done((function(_this) {
        return function(data, textStatus, jqXHR) {
          console.log(data, textStatus, jqXHR);
          $('.aadhar-input').attr('readonly', 'readonly');
          return $('.enter-otp,.ptext,.or-text').removeClass('hide');
        };
      })(this));
    });
    $('.enter-otp').submit(function(e) {
      e.preventDefault();
      return $.ajax({
        method: "POST",
        url: path + 'verify_with_otp/',
        data: {
          "aadhar": $('.otp-input').val()
        },
        dataType: 'json',
        context: document.body
      }).done((function(_this) {
        return function(data, textStatus, jqXHR) {
          $('.enter-adhar-number .input-group-btn').css({
            'visibility': 'hidden'
          });
          $('.enter-adhar-number input').css({
            'width': '113%'
          });
          if (data.data.data.name) {
            $('.username').text(data.data.data.name);
          }
          $('.enter-otp,.ptext').fadeOut('fast');
          console.log(data, textStatus, jqXHR);
          return redirect();
        };
      })(this));
    });
    $('.ptext').click(function(e) {
      $('.enter-otp,.ptext').fadeOut('fast');
      $('.verification-method-radio,.tab-content').removeClass('hide');
      return $('.verification-method-radio,.tab-content').fadeIn('fast');
    });
    $('.dg-form').submit(function(e) {
      e.preventDefault();
      return $.ajax({
        method: "POST",
        url: path + 'verify_with_details/',
        data: {
          "aadhar": $('.otp-input').val()
        },
        dataType: 'json',
        context: document.body
      }).done((function(_this) {
        return function(data, textStatus, jqXHR) {
          $('.enter-adhar-number .input-group-btn').css({
            'visibility': 'hidden'
          });
          $('.enter-adhar-number input').css({
            'width': '113%'
          });
          $('.enter-otp,.ptext').fadeOut('fast');
          return redirect();
        };
      })(this));
    });
    redirect = function() {
      setTimeout(function() {
        return window.location.href = "/eKYC/";
      }, 3500);
      return $('.redirect-container').removeClass('hide');
    };
    return $('.linkedin-login').click(function(e) {
      return console.log("amit linkedin");
    });
  })(jQuery);

}).call(this);
